import bson

class CollectionSpace(object):
    def __init__(self, conn, cs_name):
        self._conn = conn
        self._cs_name = cs_name

    def __str__(self):
        return "CollectionSpace(name=%s)" % self._cs_name

    def create_collection(self, c_name, options=None):
        if options:
            options = bson.BSON.encode(options)
        else:
            options = ''
        return self._conn.create_collection(self._cs_name, c_name, options)


    def drop_collection(self, c_name):
        return self._conn.drop_collection(self._cs_name, c_name)


#ifndef __QC__SDBAPI_H__
#define __QC__SDBAPI_H__

#include <stdlib.h>

#include <vector>
#include <string>
#include <sstream>

#include "ossTypes.h"
#include "bson/bson.hpp"
//#include "bson/bsontypes.h"

#include "common.h"

#define MD5_DIGEST_LENGTH SDB_MD5_DIGEST_LENGTH*2+1

// don't include "client_internal.h" for it .
#define CLIENT_CS_NAMESZ         127
#define CLIENT_COLLECTION_NAMESZ 127

namespace qc{

typedef void (*network_func)(CHAR* buff, size_t size);

class ConnectionContext{
public:
    CHAR* send_buff;
    INT32 send_buff_size;
    BOOLEAN endian_convert;
    CHAR* recv_buff;
    INT32 recv_buff_size;
    network_func sender;
    network_func recver;

    ConnectionContext();
    ~ConnectionContext();

    std::string str()
    {
        std::stringstream ss;
        ss << "send_buff:" << (void*)send_buff << ", size:" << send_buff_size <<
", endian:" << endian_convert;
        return ss.str();
    }

};

SINT32 steal_size(const CHAR* buff);


INT32 build_create_collection_space_command(
    ConnectionContext* conn_ctx,
    const CHAR* cs_name,
    INT32 page_size
);

INT32 build_drop_collection_space_command(
    ConnectionContext* conn_ctx,
    const CHAR* cs_name
);

INT32 build_create_collection_command(
    ConnectionContext* conn_ctx,
    const CHAR* cs_name,
    const CHAR* c_name,
    const CHAR* options
);

INT32 build_drop_collection_command(
    ConnectionContext* conn_ctx,
    const CHAR* cs_name,
    const CHAR* c_name
);

INT32 build_insert_command(
    ConnectionContext* conn_ctx,
    const CHAR* cs_name,
    const CHAR* c_name,
    const CHAR* bytes_record//,
//    bson::OID* id
);

INT32 build_bulk_insert_command(
    ConnectionContext* conn_ctx,
    const CHAR* cs_name,
    const CHAR* c_name,
    SINT32 flag,
    std::vector<std::string>& reocrds
//    const CHAR* bytes_record//,
//    bson::OID* id
);

INT32 build_cr_next_command(
    ConnectionContext* conn_ctx,
    SINT64 context_id,
    INT64 total_read

);
INT32 build_query_command(
    ConnectionContext* conn_ctx,
    const CHAR* cs_name,
    const CHAR* c_name,
    const CHAR* bytes_condition,
    const CHAR* bytes_selector,
    const CHAR* bytes_order_by,
    const CHAR* bytes_hint,
    INT64 num_to_skip,
    INT64 num_to_return,
    INT32 flag=0
);

INT32 _build_command(
    ConnectionContext* conn_ctx,
    const CHAR* cmd,
    const bson::BSONObj* arg1,
    const bson::BSONObj* arg2,
    const bson::BSONObj* arg3,
    const bson::BSONObj* arg4
);

INT32 _append_oid(const bson::BSONObj& input, bson::BSONObj& output);

} // end of namespace qc
#endif // __QC__SDBAPI_H__


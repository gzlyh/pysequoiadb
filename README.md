# 硬件环境

一台使用 `virtualbox` 虚拟出来的安装了 `Ubuntu Linux Server 64bit 14.04 LTS` 的虚拟机，宿主机器为 `Intel X64` 架构的 MacBook Pro。

* 虚拟机内存：2GB
* 虚拟机硬盘：40GB

安装好之后在终端执行 `uname -a` 可见类似输出：

```
$ uname -a
Linux ubuntu 3.16.0-52-generic #71~14.04.1-Ubuntu SMP Fri Oct 23 17:24:53 UTC 2015 x86_64 x86_64 x86_64 GNU/Linux
```

# 软件环境

首先需要更新系统软件，依次执行以下命令：

```
$ sudo aptitude update
$ sudo aptitude upgrade
```

然后安装各个依赖的软件包，执行：

```
$ sudo aptitude install build-essential ccache scons libboost-all-dev python-setuptools python-pip python-dev git
```

安装这些包需要一点时间，请耐心等待，确保安装成功。

然后需要安装 `cython`，执行：

```
$ sudo pip install cython
```

# 签出代码

## SequoiaDB

目前本驱动只支持 0.11 版本的 SequoiaDB，所以签出代码时需要签出较早的版本，以下是方法。

首先回到 HOME 目录，执行：

```
cd ~
```

然后执行：

```
$ git clone https://git.oschina.net/wangzhonnew/SequoiaDB.git
```

成功克隆到本地后执行：

```
cd SequoiaDB
```

然后执行：

```
$ git checkout 9c798f5436cdc69a210bb6c17a26622ae83f0d63
```

成功后执行：

```
$ git log
```

这时候，可以看到最上面的几行应该跟下面几行一样：

```
commit 9c798f5436cdc69a210bb6c17a26622ae83f0d63
Author: Tao Wang <taoewang@sequoiadb.com>
Date:   Sat Apr 11 14:20:15 2015 +0800

    Revision: 17223
```

至此，可以确定成功签出了特定版本的 `SequoiaDB`。

## pysequoiadb

首先回到 HOME 目录，执行：

```
cd ~
```

然后执行：

```
$ git clone https://git.oschina.net/gzlyh/pysequoiadb.git
```

成功后签出完成。

# 编译

## SequoiaDB

依次执行:

```
cd ~/SequoiaDB
scons --all
```

不需要编译旧的 `Python` Driver。

## pysequoiadb

首先进入代码目录：

```
cd ~/pysequoiadb
```

然后熟悉的编辑器打开 `setup.py`，把`ROOT_PATH` 的值改为 `SequoiaDB` 源代码所在的路径，一般是 `/home/xxx/SequoiaDB`。保存后退出

执行：

```
./build.sh
```

尝试编译一次。然后再执行：

```
python setup.py install
```

等待一段时间，成功后即编译、安装成功。

# 体验

先运行单机版本的 `SequoiaDB`，依次执行，

```
cd ~/SequoiaDB/bin
./sdbstart -p 11810 --force
```

等其运行成功，然后执行：

```
cd ~/pysequoiadb/sample
python query.py
```

可以看到运行结果：

```
query one record, using condition={'id': {'$et': 0}}
{u'Item': u'basketball', u'_id': ObjectId('56437fde52444d09b8fe6998'), u'id': 0}
query all records:
{u'Item': u'basketball', u'_id': ObjectId('56437fde52444d09b8fe6998'), u'id': 0}
{u'sport id': 2, u'_id': ObjectId('56437fde52444d09b8fe6999')}
{u'sport id': 3, u'_id': ObjectId('56437fde52444d09b8fe699a')}
{u'sport id': 4, u'_id': ObjectId('56437fde52444d09b8fe699b')}
{u'sport id': 5, u'_id': ObjectId('56437fde52444d09b8fe699c')}
{u'sport id': 6, u'_id': ObjectId('56437fde52444d09b8fe699d')}
{u'sport id': 7, u'_id': ObjectId('56437fde52444d09b8fe699e')}
{u'sport id': 8, u'_id': ObjectId('56437fde52444d09b8fe699f')}
{u'sport id': 9, u'_id': ObjectId('56437fde52444d09b8fe69a0')}
```

至此，体验完成。




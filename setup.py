#   Copyright (C) 2012-2014 SequoiaDB Ltd.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#from distutils.core import Extension, setup
from setuptools import setup, find_packages
from distutils.extension import Extension
try:
    from Cython.Build import cythonize
    from Cython.Distutils import build_ext

    has_cython = True
    cmdclass = {'build_ext': build_ext}
except ImportError:
    has_cython = False
    cmdclass = {}

import sys
import os
import glob
import shutil

if 'win32' == sys.platform:
   dlls = './pysequoiadb/*.dll'
   for file in glob.glob(dlls):
      if file.startswith('lib'):
         newname = file[3:]
      newname = file[:-3] + 'pyd'
      shutil.copy(file, newname)
   modules = ['err.prop','*.pyd'] #, '*.exp', '*.lib', 
else:
   modules = ['err.prop', '*.so']

extra_opts = {}
extra_opts['packages'] = find_packages('.')
extra_opts['package_data'] = {
	'' : ['*.h', '*.hpp', '*.pyd', '*.pyx', '*.cpp', '*.c', '*.so', 'err.prop']
	}

BOOST_PATH = '/usr/local/include'
BOOST_LIB_PATH = '/usr/local/lib'

ROOT_PATH = '/root/SequoiaDB'
ENGINE_PATH = os.path.join(ROOT_PATH, 'SequoiaDB/engine')
CLIENT_PATH = os.path.join(ROOT_PATH, 'client')

include_dirs = [
#	BOOST_PATH,
	ENGINE_PATH,
	os.path.join(CLIENT_PATH, 'include'),
	os.path.join(ENGINE_PATH, 'include'),
	os.path.join(ENGINE_PATH, 'client'),
	os.path.join(ENGINE_PATH, 'util'),
]

library_dirs = [
#	BOOST_LIB_PATH,
]

engine_files = [
	os.path.join(ENGINE_PATH, 'client/clientcpp.cpp'),
	os.path.join(ENGINE_PATH, 'client/common.c'),
	os.path.join(ENGINE_PATH, 'client/base64c.c'),

	os.path.join(ENGINE_PATH, 'client/bson/bson.c'),
	os.path.join(ENGINE_PATH, 'client/bson/encoding.c'),
	os.path.join(ENGINE_PATH, 'client/bson/numbers.c'),

	os.path.join(ENGINE_PATH, 'bson/bsonobj.cpp'),
	os.path.join(ENGINE_PATH, 'bson/base64.cpp'),
	os.path.join(ENGINE_PATH, 'bson/nonce.cpp'),
	os.path.join(ENGINE_PATH, 'bson/oid.cpp'),

	os.path.join(ENGINE_PATH, 'msg/msgAuth.cpp'),
	os.path.join(ENGINE_PATH, 'msg/msgCatalog.cpp'),
	os.path.join(ENGINE_PATH, 'msg/msgMessage.cpp'),
	os.path.join(ENGINE_PATH, 'msg/msgReplicator.cpp'),


	os.path.join(ENGINE_PATH, 'oss/oss.cpp'),
	os.path.join(ENGINE_PATH, 'oss/ossErr.cpp'),
	os.path.join(ENGINE_PATH, 'oss/ossIO.cpp'),
	os.path.join(ENGINE_PATH, 'oss/ossSocket.cpp'),
	os.path.join(ENGINE_PATH, 'oss/ossMem.cpp'),
	os.path.join(ENGINE_PATH, 'oss/ossPath.cpp'),
	os.path.join(ENGINE_PATH, 'oss/ossPrimitiveFileOp.cpp'),
	os.path.join(ENGINE_PATH, 'oss/ossUtil.cpp'),
	os.path.join(ENGINE_PATH, 'oss/ossVer.cpp'),

	os.path.join(ENGINE_PATH, 'util/utilStr.cpp'),

	os.path.join(ENGINE_PATH, 'pd/pd.cpp'),
	os.path.join(ENGINE_PATH, 'pd/pdComponents.cpp'),
	os.path.join(ENGINE_PATH, 'pd/pdFunctionList.cpp'),
	os.path.join(ENGINE_PATH, 'pd/pdTrace.cpp'),
]

#print engine_files

py_sdb_files = [
	'pysequoiadb/sdb/_sdbapi.cpp',
]

if has_cython:
    py_sdb_files.append('pysequoiadb/sdb/sdbapi.pyx')
else:
    py_sdb_files.append('pysequoiadb/sdb/sdbapi.cpp')

source_files = engine_files + py_sdb_files

for f in source_files:
    if not os.path.isfile(f):
        raise RuntimeError('%s is not exist.' % f)

extentions = [
    Extension(
        'pysequoiadb.sdb.sdbapi',
	source_files,
	language = 'c++', 
	include_dirs = include_dirs,
	libraries = [
		'boost_filesystem',
		'boost_system',
		'boost_thread',
		'boost_program_options',
	],
	library_dirs = library_dirs,
    ),
]

#extra_opts['ext_modules'] = cythonize(
#    extentions,
#)

extra_opts['ext_modules'] = extentions

setup(name = 'pysequoiadb',
      cmdclass = cmdclass,
      version = '1.0',
      author = 'SequoiaDB Inc.',
      license = 'GNU Affero GPL',
      description = 'This is a sequoiadb python driver use adapter package',
      url = 'http://www.sequoiadb.com',
      **extra_opts)

if 'win32' == sys.platform:
   pyds = './pysequoiadb/*.pyd'
   for file in glob.glob(pyds):
      os.remove(file)
